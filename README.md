# viem co tu cung

<p>Bệnh phụ khoa là một thuật ngữ để chỉ các bệnh ở bộ phận sinh dục nữ giới, có ảnh hưởng trực tiếp đến khả năng sinh sản sau này. Trong đó, tiêu biểu là bệnh viêm cổ tử cung &ndash; một bệnh phụ khoa phổ biến thường hay gặp. Có rất nhiều bạn nữ gửi thắc mắc về bệnh, tiêu biểu như trường hợp sau:</p>

<p>&ldquo;Chào bác sĩ, em năm nay 27 tuổi. Dạo gần đây em thấy sức khỏe vùng kín của em không được tốt, khí hư ra nhiều hơn, màu sắc nhìn rất lạ, cảm thấy ngứa và nóng rát. Tình cảm đời sống vợ chồng cũng nhạt hơn do quan hệ đau rát, thậm chí có lúc còn ra máu sau khi quan hệ. Hôm trước, khi đi kiểm tra sức khỏe định kỳ theo công ty ở bệnh viện, em có khám qua thì các bác sĩ bảo em là đang bị viêm cổ tử cung, cần phải áp dụng điều trị&nbsp; ngoại khoa để chữa nhanh nhất và dứt điểm tình trạng bệnh. Vậy các bác sĩ có thể tư vấn cho em cách chữa viêm cổ tử cung bằng ngoại khoa là gì? Bao gồm những phương pháp nào được không ạ? Em cảm ơn bác sĩ, mong sớm nhận lại được hồi âm ạ&rdquo;.</p>

<p>( Chị T. Hương &ndash; Hà Nội)</p>


